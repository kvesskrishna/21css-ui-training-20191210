var numbers = [1, 2, 3, 4, 5, 6];
// var evens = [...numbers];
// // var length = evens.length;
// for (let index = 0; index < numbers.length; index++) {
//   const element = numbers[index];
//   console.log(element);
//   if (element % 2 == 0) {
//     evens.push(element);
//   }
//   // element % 2 == 0 ? evens.push(element) : evens.push(null);
// }
var evens = numbers.filter(function(number) {
  return number % 2 == 0;
});
console.log(evens, numbers);

var double = numbers.map(function(number) {
  return number * 2;
});
console.log(double);

var users = [
  { name: "john", email: "john@example.com", salary: 20000 },
  { name: "doe", email: "doe@example.com", salary: 90000 },
  { name: "smith", email: "smith@example.com", salary: 344433 }
];
var moresalary = users.filter(function(user) {
  return user.salary > 20000; // return matching this condition
});
console.log(moresalary);

var deductedsalary = users.map(function(user) {
  user.salary = user.salary - 5000; // change the value
  return user;
});
console.log(deductedsalary);

// var rows = '';
// users.forEach(user => {
//   console.log(user.email);
//   rows += '<td>'+user.name+'</td><td>'+user.email+'</td><td>'+user.salary+'</td>'
// });
// document.getElementById('table').innerHTML = rows;
