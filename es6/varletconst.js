var a = 5;
let b = 6;
const c = 7;

c = 11;
if (a > 6) {
  let d = 8; // block scope
  var e = 10; // function scope
  console.log(a, b, c, d);
}
console.log(a, b, c, d, e);
