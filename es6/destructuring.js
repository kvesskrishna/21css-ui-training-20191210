"use strict";
exports.__esModule = true;
var fruits = ["apple", "grapes"];
// fruit1 = fruits[0];
// fruit2 = fruits[1];
var fruit1 = fruits[0], fruit2 = fruits[1], fruit3 = fruits[2];
// console.log(fruit1, fruit2, fruit3);
exports["default"] = fruits;
// export { fruit1 };
