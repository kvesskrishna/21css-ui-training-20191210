const fruits = ["apple", "grapes"];

// fruit1 = fruits[0];
// fruit2 = fruits[1];

const [fruit1, fruit2, fruit3] = fruits;

// console.log(fruit1, fruit2, fruit3);

export default fruits;
// export { fruit1 };
