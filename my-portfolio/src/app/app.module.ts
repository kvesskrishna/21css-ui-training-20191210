import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { UsersComponent } from "./users/users.component";
import { LayoutComponent } from "./layout/layout.component";
import { HeaderComponent } from "./header/header.component";
import { SidenavComponent } from "./sidenav/sidenav.component";
import { MainComponent } from "./main/main.component";
import { FooterComponent } from "./footer/footer.component";
import { StringifyNumberPipe } from "./pipes/stringify-number.pipe";
import { AboutComponent } from "./about/about.component";
import { ContactComponent } from "./contact/contact.component";
import { UserDetailComponent } from "./users/user-detail/user-detail.component";
import { HttpusersComponent } from "./httpusers/httpusers.component";
import { HobbiesComponent } from "./main/hobbies/hobbies.component";
import { FoodComponent } from "./main/food/food.component";
import { FamilyComponent } from "./main/family/family.component";
import { TemplateDrivenFormComponent } from "./template-driven-form/template-driven-form.component";
import { ReactiveFormsComponent } from "./reactive-forms/reactive-forms.component";
@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    LayoutComponent,
    HeaderComponent,
    SidenavComponent,
    MainComponent,
    FooterComponent,
    StringifyNumberPipe,
    AboutComponent,
    ContactComponent,
    UserDetailComponent,
    HttpusersComponent,
    HobbiesComponent,
    FoodComponent,
    FamilyComponent,
    TemplateDrivenFormComponent,
    ReactiveFormsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
