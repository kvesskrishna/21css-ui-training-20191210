import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { users } from "./../../../assets/users";
@Component({
  selector: "app-user-detail",
  templateUrl: "./user-detail.component.html",
  styleUrls: ["./user-detail.component.css"]
})
export class UserDetailComponent implements OnInit {
  constructor(private route: ActivatedRoute) {}
  id: any;
  userlist = users;
  filtereduser: any;
  ngOnInit(): void {
    this.route.params.subscribe(data => {
      console.log(data.userid);
      this.id = data.userid;
      const filtereduser = this.userlist.filter(user => user.id == this.id);
      console.log(filtereduser[0]);
      this.filtereduser = filtereduser[0];
    });
  }
}
