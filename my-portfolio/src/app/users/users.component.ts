import { Component, OnInit } from "@angular/core";
import { users } from "./../../assets/users";
import { NameService } from "../services/name.service";

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.css"]
})
export class UsersComponent implements OnInit {
  users = [];
  primary = false;
  danger = false;
  username = "";
  constructor(private nameservice: NameService) {}

  ngOnInit(): void {
    this.users = users;
    this.username = this.nameservice.getUser();
  }
  setClass(c) {
    console.log(c);
    switch (c) {
      case "danger":
        this.danger = true;
        this.primary = false;
        break;
      case "primary":
        this.primary = true;
        this.danger = false;
        break;

      default:
        break;
    }
  }
}
