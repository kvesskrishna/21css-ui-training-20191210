import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
@Component({
  selector: "app-template-driven-form",
  templateUrl: "./template-driven-form.component.html",
  styleUrls: ["./template-driven-form.component.css"]
})
export class TemplateDrivenFormComponent implements OnInit {
  constructor(private http: HttpClient) {}
  error = false;
  message = "";
  ngOnInit(): void {}
  submitForm(value) {
    console.log(value);
    if (typeof value.fullname == "undefined") {
      this.error = true;
      this.message = "full name is required";
    } else {
      this.http.post("api endpoint", value).subscribe(data => {
        console.log(data);
      });
      this.error = false;
      this.message = "form submitted successfully";
    }
  }
}
