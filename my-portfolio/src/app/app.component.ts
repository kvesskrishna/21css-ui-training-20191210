import { Component } from "@angular/core";
@Component({
  selector: "myapp-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  name = "krishna";
  age = 34;
  course = "Angular";
  inputval = "";
  showMessage = true;
  paracolor = "blue";

  updateName(e) {
    console.log(e.target.value);
    this.inputval = e.target.value;
  }
  btnHandler() {
    console.log("btn clicked");
    this.name = this.inputval;
  }
  toggleMessage() {
    this.showMessage = !this.showMessage;
  }
}
