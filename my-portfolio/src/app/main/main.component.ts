import { Component, OnInit } from "@angular/core";
import { NameService } from "../services/name.service";

@Component({
  selector: "app-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.css"]
})
export class MainComponent implements OnInit {
  content = `Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum ducimus quis tenetur. Sapiente obcaecati eos
        delectus, id omnis modi natus perferendis eligendi ad placeat, voluptatem dicta optio, nemo odit voluptates!
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor ea ducimus placeat sint corrupti pariatur laborum
        nihil. Esse ullam, commodi et aperiam in harum, sapiente quibusdam ab vel consectetur ipsam? Lorem ipsum dolor
        sit, amet consectetur adipisicing elit. Enim autem corporis eaque consequuntur possimus voluptatum, doloremque
        atque quod quis delectus, vel repudiandae natus, nam iste sapiente vero at. Voluptatibus, quaerat`;
  price = 23;
  today = new Date();
  num = 0;
  name = "";
  constructor(private nameservice: NameService) {}

  ngOnInit(): void {}

  setName() {
    console.log(this.name);
    this.nameservice.setUser(this.name);
  }
}
