import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class NameService {
  constructor() {}
  name = "";
  setUser(name) {
    this.name = name;
  }
  getUser() {
    return this.name;
  }
}
