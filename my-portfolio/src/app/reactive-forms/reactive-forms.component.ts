import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
@Component({
  selector: "app-reactive-forms",
  templateUrl: "./reactive-forms.component.html",
  styleUrls: ["./reactive-forms.component.css"]
})
export class ReactiveFormsComponent implements OnInit {
  constructor(private fb: FormBuilder) {}

  regForm: FormGroup;
  submitted = false;

  ngOnInit(): void {
    this.regForm = this.fb.group({
      fullname: ["", [Validators.required, Validators.minLength(6)]],
      email: ["", [Validators.email, Validators.required]],
      password: ["", [Validators.required, Validators.minLength(8)]]
    });
  }

  get f() {
    return this.regForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.regForm.invalid) {
      console.log("invalid form");
      return;
    }
    console.log(this.regForm.value);
  }
}
