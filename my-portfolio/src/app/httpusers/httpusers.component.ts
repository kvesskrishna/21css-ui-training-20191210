import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
@Component({
  selector: "app-httpusers",
  templateUrl: "./httpusers.component.html",
  styleUrls: ["./httpusers.component.css"]
})
export class HttpusersComponent implements OnInit {
  constructor(private http: HttpClient) {}
  users: any;
  ngOnInit(): void {
    this.http
      .get("https://jsonplaceholder.typicode.com/users")
      .subscribe(data => {
        console.log(data);
        this.users = data;
      });
  }
}
