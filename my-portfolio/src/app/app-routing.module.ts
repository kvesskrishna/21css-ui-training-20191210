import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MainComponent } from "./main/main.component";
import { AboutComponent } from "./about/about.component";
import { ContactComponent } from "./contact/contact.component";
import { UsersComponent } from "./users/users.component";
import { UserDetailComponent } from "./users/user-detail/user-detail.component";
import { HttpusersComponent } from "./httpusers/httpusers.component";
import { HobbiesComponent } from "./main/hobbies/hobbies.component";
import { FoodComponent } from "./main/food/food.component";
import { FamilyComponent } from "./main/family/family.component";
import { TemplateDrivenFormComponent } from "./template-driven-form/template-driven-form.component";
import { ReactiveFormsComponent } from "./reactive-forms/reactive-forms.component";

const routes: Routes = [
  { path: "", redirectTo: "/home", pathMatch: "full" },
  {
    path: "home",
    component: MainComponent,
    children: [
      {
        path: "hobbies",
        component: HobbiesComponent
      },
      {
        path: "food",
        component: FoodComponent
      },
      { path: "family", component: FamilyComponent }
    ]
  },
  { path: "about", component: AboutComponent },
  { path: "users", component: HttpusersComponent },
  { path: "templatedrivenform", component: TemplateDrivenFormComponent },
  { path: "reactiveform", component: ReactiveFormsComponent },
  { path: "userDetail/:userid", component: UserDetailComponent },
  {
    path: "test",
    loadChildren: () => import("./test/test.module").then(m => m.TestModule)
  },
  { path: "contact", component: ContactComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
