import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "stringifyNumber"
})
export class StringifyNumberPipe implements PipeTransform {
  transform(value: unknown, ...args: unknown[]): unknown {
    console.log(args[0]);
    switch (value) {
      case 0:
        if (args[0] == "capital") {
          return "Zero".toUpperCase();
        } else if (args[0] == "small") {
          return (args[1] ? args[1] : "hola") + " Zero".toLowerCase();
        }
        return "Zero";
        break;
      case 1:
        return "One";
        break;
      case 2:
        return "Two";
        break;
      case 3:
        return "Three";
        break;
      case 4:
        return "Four";
        break;
      case 5:
        return "Five";
        break;

      default:
        return "Value Greater than 5";
        break;
    }
  }
}
