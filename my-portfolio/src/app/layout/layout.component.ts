import { Component, OnInit } from "@angular/core";
import { users } from "./../../assets/users";
@Component({
  selector: "app-layout",
  templateUrl: "./layout.component.html",
  styleUrls: ["./layout.component.css"]
})
export class LayoutComponent implements OnInit {
  constructor() {}
  userslist = users;
  layout = 4;

  ngOnInit(): void {}
  changeLayout(l) {
    this.layout = l;
  }
}
