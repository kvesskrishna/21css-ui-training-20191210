export const users = [
  {
    id: 1,
    name: "krishna",
    email: "krishna@gmail.com"
  },
  {
    id: 2,
    name: "john",
    email: "fasdf@gmail.com"
  },
  {
    id: 3,
    name: "alex",
    email: "asdf@gmail.com"
  },
  {
    id: 4,
    name: "doe",
    email: "doe@gmail.com"
  },
  {
    id: 5,
    name: "alex",
    email: "asdf@gmail.com"
  },
  {
    id: 6,
    name: "doe",
    email: "doe@gmail.com"
  }
];
